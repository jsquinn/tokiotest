// server starts with listening port and stdio listener
// connecting sockets are added to a list
// stdio string s are queued an async sent to all connected sockets

// https://rust-lang.github.io/async-book/
// cd ~/.cargo/registry/src/github.com-1ecc6299db9ec823/tokio-1.35.1/
// https://docs.rs/tokio-util/latest/tokio_util/sync/struct.CancellationToken.html
// https://doc.rust-lang.org/std/future/trait.Future.html
// https://news.ycombinator.com/item?id=26406989
// https://eta.st/2021/03/08/async-rust-2.html
// https://without.boats/tags/io-uring/
// https://docs.rs/tokio/latest/tokio/sync/broadcast/index.html

use tokio::io::AsyncBufReadExt;
use tokio::net::TcpListener;
use std::sync::Arc;
use std::sync::Mutex;
use tokio::io::AsyncWriteExt;

type IOTask = tokio::task::JoinHandle<std::io::Result<()>>;

struct Server {
    listener: IOTask,
    clients: Arc<Mutex<Vec<IOTask>>>,
    broadcast: Arc<tokio::sync::broadcast::Sender<String>>,
}

impl Server {
    fn new() -> Server {
        let clients = Arc::new(Mutex::new(Vec::new()));
        let (tx, _rx) = tokio::sync::broadcast::channel::<String>(256);
        let broadcast = Arc::new(tx);

        let listener_clients = clients.clone();
        let listener_broadcast = broadcast.clone();
        let listener = tokio::spawn(async move {
            let listener = TcpListener::bind("127.0.0.1:7878").await?;
            println!("listener bound");
            loop {
                let (mut socket, _) = listener.accept().await?;
                println!("socket accepted");
                let mut new_rx = listener_broadcast.subscribe();
                let new_client = tokio::spawn(async move {
                        loop {
                            if let Ok(s) = new_rx.recv().await {
                                socket.write(s.as_bytes()).await?;
                            }
                        }
                    });
                listener_clients.lock().unwrap().push(new_client);
            }
        });
        Server{listener: listener, clients: clients, broadcast: broadcast }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>>{
    println!("Hello, world!");
    let server = Server::new();
    let mut reader = tokio::io::BufReader::new(tokio::io::stdin());
    println!("Please respond...");
    let mut line = String::new();
    loop {
        reader.read_line(&mut line).await?;
        if line == "quit\n" {
            break;
        } else {
            if let Err(se) = server.broadcast.send(line.clone()) {
                println!("SendError {}", se);
            }
        }
        line.clear();
    }

    println!("bye...");

    server.listener.abort();
    match tokio::join!(server.listener).0 {
        Ok(result) => match result {
            Ok(()) =>println!("...listener dropped"),
            Err(e) => println!("...listener failed {}", e),
        },
        Err(je) => println!("...listener join failed {}", je),
    }

    for c in server.clients.lock().unwrap().iter_mut() {
        c.abort();
        match tokio::join!(c).0 {
            Ok(result) => match result {
                Ok(()) =>println!("...client dropped"),
                Err(e) => println!("...client failed {}", e),
            },
            Err(je) => println!("...client join failed {}", je),
        }
    }
    Ok(())
}

